create table book (
  id serial PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  author_id int REFERENCES author(id) ON DELETE CASCADE ON UPDATE CASCADE,
  description TEXT,
  year int not null,
  quantity int not null,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
create table author (
  id serial PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);


insert into book (title, author_id, description, year, quantity)
values ('title book', 1, 'description book', 2019, 5);

create table customer (
  id serial primary key,
  name varchar(100) not null,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

create table borrowing_history (
  id serial primary key ,
  customer_id int REFERENCES customer(id) ON DELETE CASCADE ON UPDATE CASCADE,
  book_id int REFERENCES book(id) ON DELETE CASCADE ON UPDATE CASCADE,
  borrowing_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  return_date DATE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
