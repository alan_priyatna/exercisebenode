import request from 'supertest';
import app from '../../src/app';

describe('BookController', () => {
  describe('GET /books', () => {
    beforeEach(async () => {
      const { Book, Author } = app.locals.models;
      await Author.destroy({
        truncate: true,
        cascade: true
      });
      await Book.destroy({
        truncate: true,
        cascade: true
      });
    });
    it('should return true when the respondse array is same with the expected array', async () => {
      const { Book, Author } = app.locals.models;
      const savedAuthor = await Author.create({
        name: 'name author'
      });
      const savedBook = await Book.create({
        title: 'title book',
        year: 2019,
        description: 'description book',
        quantity: 5,
        authorId: savedAuthor.id
      });
      const expectedResult = {
        ...(JSON.parse(JSON.stringify(savedBook))),
        author: JSON.parse(JSON.stringify(savedAuthor))
      };


      const respondse = await request(app)
        .get('/books')
        .expect(200);
      expect(respondse.body).toEqual([expectedResult]);
    });
    it('should return list of book when quantity more than 0', async () => {
      const { Book, Author } = app.locals.models;
      const savedAuthor = await Author.create({
        name: 'name author'
      });
      const savedBook1 = await Book.create({
        title: 'title book',
        year: 2019,
        description: 'description book',
        quantity: 5,
        authorId: savedAuthor.id
      });
      await Book.create({
        title: 'title book 2',
        year: 2019,
        description: 'description book',
        quantity: 0,
        authorId: savedAuthor.id
      });
      const expectedResult = {
        ...(JSON.parse(JSON.stringify(savedBook1))),
        author: JSON.parse(JSON.stringify(savedAuthor))
      };

      const respondse = await request(app)
        .get('/books?quantity[gt]=0')
        .expect(200);
      expect(respondse.body).toEqual([expectedResult]);
    });
  });
});
