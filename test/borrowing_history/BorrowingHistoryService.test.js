import BorrowingHistoryService from '../../src/borrowing_history/BorrowingHistoryService';
import BookNotFoundError from '../../src/error/BookNotFoundError';

describe('BorrowingHistoryService', () => {
  describe('#saveById', () => {
    let models;
    let borrowingHistoryService;
    beforeEach(() => {
      models = {
        Book: {
          findBookId: jest.fn()
        },
        Customer: {
          findCustomer: jest.fn()
        },
        BorrowingHistory: {
          create: jest.fn()
        }
      };
      borrowingHistoryService = new BorrowingHistoryService(models);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return value saved borrowing history for that bookId and customerId', async () => {
      const mockBook = {
        title: 'Book Title',
        authorId: 1,
        year: 2019,
        quantity: 4,
        id: 1,
        description: 'asdf',
        hasQuantity: jest.fn(),
        addQuantityBy: jest.fn()
      };
      const mockCustomer = {
        name: 'authorName',
        id: 1
      };
      const mockBorrowingHistory = {
        bookId: 1,
        id: 1,
        customerId: 1
      };
      models.Book.findBookId.mockResolvedValue(mockBook);
      mockBook.hasQuantity.mockResolvedValue(true);
      mockBook.addQuantityBy.mockImplementation((number) => {
        mockBook.quantity += number;
        Promise.resolve();
      });
      models.Customer.findCustomer.mockResolvedValue(mockCustomer);
      models.BorrowingHistory.create.mockResolvedValue(mockBorrowingHistory);

      const result = await borrowingHistoryService.saveById(mockBook.id, mockCustomer.id);

      expect(result)
        .toEqual(mockBorrowingHistory);
    });

    it('should return book not found error when the book is not in the database', async () => {
      const mockCustomer = {
        name: 'authorName',
        id: 1
      };
      const mockBook = {
        title: 'Book Title',
        authorId: 1,
        year: 2019,
        quantity: 4,
        id: 1,
        description: 'asdf',
        hasQuantity: jest.fn(),
        addQuantityBy: jest.fn()
      };
      models.Book.findBookId.mockResolvedValue();
      models.Customer.findCustomer.mockResolvedValue(mockCustomer);

      await expect(borrowingHistoryService.saveById(mockBook.id, mockCustomer.id))
        .rejects.toEqual(new BookNotFoundError());
    });
  });
});
