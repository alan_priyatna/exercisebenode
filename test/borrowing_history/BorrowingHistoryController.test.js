import request from 'supertest';
import app from '../../src/app';

describe('BorrowingHistoryController', () => {
  beforeEach(async () => {
    const {
      Book, Author, BorrowingHistory, Customer
    } = app.locals.models;
    await Author.destroy({
      truncate: true,
      cascade: true
    });

    await Book.destroy({
      truncate: true,
      cascade: true
    });

    await BorrowingHistory.destroy({
      truncate: true,
      cascade: true
    });

    await Customer.destroy({
      truncate: true,
      cascade: true
    });
  });

  describe('POST /borrowing-histories', () => {
    it('should has data in borrowing history and same with the inserted data', async () => {
      const {
        Book, Author, Customer
      } = app.locals.models;
      const savedAuthor = await Author.create({
        name: 'name author'
      });
      const savedBook = await Book.create({
        title: 'title book',
        year: 2019,
        description: 'description book',
        quantity: 5,
        authorId: savedAuthor.id
      });
      const savedCustomer = await Customer.create({
        name: 'name Customer'
      });

      const response = await request(app)
        .post('/borrowing-histories')
        .send({
          bookId: savedBook.id,
          customerId: savedCustomer.id
        })
        .expect(201);

      const expectedBorrowingHistory = {
        bookId: savedBook.id,
        customerId: savedCustomer.id
      };

      expect(response.body).toMatchObject((expectedBorrowingHistory));
    });

    it('should throw book not found error', async () => {
      const { Customer } = app.locals.models;
      const savedCustomer = await Customer.create({
        name: 'name Customer'
      });

      await request(app)
        .post('/borrowing-histories')
        .send({
          bookId: 123,
          customerId: savedCustomer.id
        })
        .expect(404);
    });

    it('should throw customer not found error', async () => {
      const { Author, Book } = app.locals.models;
      const savedAuthor = await Author.create({
        name: 'name author'
      });
      const savedBook = await Book.create({
        title: 'title book',
        year: 2019,
        description: 'description book',
        quantity: 5,
        authorId: savedAuthor.id
      });

      await request(app)
        .post('/borrowing-histories')
        .send({
          bookId: savedBook.id,
          customerId: 1234
        })
        .expect(404);
    });

    it('should return error Bad Request and the message when the bookId is not provided', async () => {
      const response = await request(app)
        .post('/borrowing-histories')
        .send({
          customerId: 1
        })
        .expect(400);
      console.log(response);
      expect(response.body.message).toEqual('"bookId" is required');
    });
  });

  describe('PUT /borrowing-histories/:id', () => {
    it('should return error 404 when borrowing history not found', async () => {
      await request(app)
        .put('/borrowing-histories/1')
        .expect(404);
    });

    it('should return true when the book is returned', async () => {
      const {
        Book, Author, Customer
      } = app.locals.models;
      const savedAuthor = await Author.create({
        name: 'name author'
      });
      const savedBook = await Book.create({
        title: 'title book',
        year: 2019,
        description: 'description book',
        quantity: 5,
        authorId: savedAuthor.id
      });
      const savedCustomer = await Customer.create({
        name: 'name Customer'
      });
      const { borrowingHistory } = app.locals.services;
      const savedBorrowingHistory = await borrowingHistory.saveById(savedBook.id, savedCustomer.id);


      const response = await request(app)
        .put(`/borrowing-histories/${savedBorrowingHistory.id}`)
        .expect(200);

      expect(response.body.returnDate).not.toEqual(null);
    });
  });
});
