// eslint-disable-next-line no-unused-vars
const middlewareError = (error, req, res, _) => {
  const errorResponse = {
    statusCode: error.statusCode,
    message: error.message
  };
  console.error(error);
  return res.status(error.statusCode).json(errorResponse);
};

export default middlewareError;
