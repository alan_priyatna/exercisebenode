import Joi from '@hapi/joi';

module.exports = {
  borrowingHistorySchemas: {
    postSchema: Joi.object().keys({
      bookId: Joi.number().required(),
      customerId: Joi.number().required()
    }),
    putSchema: Joi.object().keys({
      id: Joi.number().required()
    })
  }
};
