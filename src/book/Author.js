import { Model, DataTypes } from 'sequelize';

/**
 * Represent a model to manage author table
 */
class Author extends Model {
  static init(sequelize) {
    return super.init({
      name: {
        allowNull: false,
        type: String,
        field: 'name'
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at'
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'updated_at'
      }
    }, {
      sequelize,
      modelName: 'author',
      tableName: 'author'
    });
  }
}

export default Author;
