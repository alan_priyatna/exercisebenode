import * as express from 'express';
import { Op } from 'sequelize';

/**
 * Represent the end point of the books
 */
export default class BookController {
  constructor(app) {
    this._app = app;
    this._router = express.Router();
    this._showBooks = this._showBooks.bind(this);
  }

  registerController() {
    this._app.use('/books', this._router);
    this._router.get('/', this._showBooks);
  }

  async _showBooks(req, res) {
    const { Book } = this._app.locals.models;
    const reqQuery = req.query;
    const queryProperties = Object.keys(reqQuery);
    const queries = {};
    queryProperties.forEach((queryProperty) => {
      const queryValue = {};
      Object.keys(reqQuery[queryProperty]).forEach((subQueryProperty) => {
        queryValue[Op[subQueryProperty]] = reqQuery[queryProperty][subQueryProperty];
      });
      queries[queryProperty] = queryValue;
    });
    const result = await Book.findBooks(this._app.locals.models, queries);
    return res.json(result);
  }
}
