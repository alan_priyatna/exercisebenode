import { Model, DataTypes, INTEGER, Op } from 'sequelize';

/**
 * Represent a model to manage book table
 */
class Book extends Model {
  static init(sequelize) {
    return super.init({
      title: {
        allowNull: false,
        type: String,
        field: 'title'
      },
      authorId: {
        allowNull: false,
        type: INTEGER,
        field: 'author_id'
      },
      year: {
        allowNull: false,
        type: INTEGER,
        field: 'year'
      },
      quantity: {
        allowNull: false,
        type: INTEGER,
        field: 'quantity'
      },
      description: {
        allowNull: false,
        type: String,
        field: 'description'
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at'
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'updated_at'
      }
    }, {
      sequelize,
      modelName: 'book',
      tableName: 'book'
    });
  }

  static associate(models) {
    const { Book: BookModel, Author: AuthorModel } = models;
    BookModel.belongsTo(AuthorModel, { foreignKey: 'authorId' });
  }

  static async findBooks(models, queries) {
    const { Book: BookModel, Author: author } = models;
    const bookData = await BookModel.findAll({
      where: queries,
      include: [
        {
          model: author,
          required: true
        }
      ]
    });
    return bookData;
  }

  static async findBookId(id) {
    const bookData = await Book.findOne({
      where: {
        id
      }
    });
    return bookData;
  }

  hasQuantity() {
    return this.getDataValue('quantity') > 0;
  }

  async addQuantityBy(number) {
    await this.update({
      quantity: this.getDataValue('quantity') + number
    });
  }
}

export default Book;
