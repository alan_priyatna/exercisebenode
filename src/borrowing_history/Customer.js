import { Model, DataTypes } from 'sequelize';

/**
 * Represent a model to manage customer table
 */
class Customer extends Model {
  static init(sequelize) {
    return super.init({
      name: {
        allowNull: false,
        type: String,
        field: 'name'
      },

      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at'
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'updated_at'
      }
    }, {
      sequelize,
      modelName: 'customer',
      tableName: 'customer'
    });
  }
  static async findCustomer(id) {
    const customerData = await Customer.findOne({
      where: {
        id
      }
    });
    return customerData;
  }
}

export default Customer;
