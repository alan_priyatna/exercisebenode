import { Model, DataTypes, INTEGER } from 'sequelize';

/**
 * Represent a model to manage borrowing_history table
 */
class BorrowingHistory extends Model {
  static init(sequelize) {
    return super.init({
      customerId: {
        allowNull: false,
        type: INTEGER,
        field: 'customer_id'
      },

      bookId: {
        allowNull: false,
        type: INTEGER,
        field: 'book_id'
      },

      borrowingDate: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'borrowing_date'
      },

      returnDate: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'return_date'
      },

      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'created_at'
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: 'updated_at'
      }
    }, {
      sequelize,
      modelName: 'borrowing_history',
      tableName: 'borrowing_history'
    });
  }

  static associate(models) {
    const {
      Book: BookModel,
      Customer: CustomerModel,
      BorrowingHistory: BorrowingHistoryModel
    } = models;
    BorrowingHistoryModel.belongsTo(BookModel, { foreignKey: 'bookId' });
    BorrowingHistoryModel.belongsTo(CustomerModel, { foreignKey: 'customerId' });
  }

  static async findBorrowingHistory(id) {
    const borrowingData = await BorrowingHistory.findOne({
      where: {
        id
      }
    });
    return borrowingData;
  }

  async updateData() {
    await this.update({
      returnDate: Date.now()
    });
  }
}

export default BorrowingHistory;
