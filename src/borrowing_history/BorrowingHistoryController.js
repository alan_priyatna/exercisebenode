import * as express from 'express';
import { borrowingHistorySchemas } from '../middleware/schemesBorrowingHistory';
import { validationRequest } from '../middleware/validationRequest';
import withHandleError from '../middleware/withHandleError';
/**
 Represent the end point of the borrowing_histories
 */
export default class BorrowingHistoryController {
  constructor(app) {
    this._app = app;
    this._router = express.Router();
    this._postHistory = this._postHistory.bind(this);
    this._putHistory = this._putHistory.bind(this);
    this._services = this._app.locals.services;
  }

  registerController() {
    this._app.use('/borrowing-histories', this._router);
    this._router.post('/', validationRequest(borrowingHistorySchemas.postSchema, 'body'), withHandleError(this._postHistory));
    this._router.put('/:id', validationRequest(borrowingHistorySchemas.putSchema, 'params'), withHandleError(this._putHistory));
  }

  async _postHistory(req, res) {
    const { bookId, customerId } = req.body;
    const { borrowingHistory } = this._services;
    const resultService = await borrowingHistory.saveById(bookId, customerId);
    return res.status(201).json(resultService);
  }

  async _putHistory(req, res) {
    const { id } = req.params;
    const { borrowingHistory } = this._services;
    const resultService = await borrowingHistory.returnById(id);
    return res.status(200).json(resultService);
  }
}
