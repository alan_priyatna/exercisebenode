import BookNotFoundError from '../error/BookNotFoundError';
import CustomerNotFoundError from '../error/CustomerNotFoundError';
import BookNotInStockError from '../error/BookNotInStockError';
import BorrowingHistoryNotFound from '../error/BorrowingHistoryNotFound';

export default class BorrowingHistoryService {
  constructor(models) {
    this._models = models;
  }

  async saveById(bookId, customerId) {
    const { Book, Customer, BorrowingHistory } = this._models;
    const bookData = await Book.findBookId(bookId);

    const customerData = await Customer.findCustomer(customerId);

    if (!bookData) {
      throw new BookNotFoundError();
    }

    if (!customerData) {
      throw new CustomerNotFoundError();
    }

    if (!bookData.hasQuantity()) {
      throw new BookNotInStockError();
    }

    const result = await BorrowingHistory.create({
      bookId,
      customerId,
      borrowingDate: Date.now()
    });


    const additionNumber = 1;
    await bookData.addQuantityBy(additionNumber);
    return result;
  }

  async returnById(id) {
    const { Book, BorrowingHistory } = this._models;
    const selectedBorrowingHistory = await BorrowingHistory.findBorrowingHistory(id);
    if (!selectedBorrowingHistory) {
      throw new BorrowingHistoryNotFound();
    }

    const bookData = await Book.findBookId(selectedBorrowingHistory.bookId);
    await selectedBorrowingHistory.updateData();
    const additionNumber = -1;
    await bookData.addQuantityBy(additionNumber);
    return selectedBorrowingHistory;
  }
}
