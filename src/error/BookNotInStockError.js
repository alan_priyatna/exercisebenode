import createError from 'http-errors';

const { Forbidden } = createError;

export default class BookNotInStockError extends Forbidden {
  constructor() {
    const MESSAGE = 'Book Not in Stock';
    super(MESSAGE);
  }
}
