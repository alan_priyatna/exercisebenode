import createError from 'http-errors';

const { NotFound } = createError;

export default class CustomerNotFoundError extends NotFound {
  constructor() {
    const MESSAGE = 'Customer Not Found';
    super(MESSAGE);
  }
}
