import createError from 'http-errors';

const { NotFound } = createError;

export default class BorrowingHistoryNotFoundError extends NotFound {
  constructor() {
    const MESSAGE = 'Borrowing History Not Found';
    super(MESSAGE);
  }
}
