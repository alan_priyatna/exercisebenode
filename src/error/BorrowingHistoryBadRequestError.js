import createError from 'http-errors';

const { BadRequest } = createError;

export default class BorrowingHistoryBadRequestError extends BadRequest {
  constructor() {
    const MESSAGE = 'Bad Request';
    super(MESSAGE);
  }
}
