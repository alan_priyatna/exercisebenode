import createError from 'http-errors';

const { NotFound } = createError;

export default class BookNotFoundError extends NotFound {
  constructor() {
    const MESSAGE = 'Book Not Found';
    super(MESSAGE);
  }
}
