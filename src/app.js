import bodyParser from 'body-parser';
import express from 'express';
import BookController from './book/BookController';
import * as database from './database';
import config from '../config';
import Book from './book/Book';
import Author from './book/Author';
import BorrowingHistory from './borrowing_history/BorrowingHistory';
import Customer from './borrowing_history/Customer';
import BorrowingHistoryController from './borrowing_history/BorrowingHistoryController';
import BorrowingHistoryService from './borrowing_history/BorrowingHistoryService';
import middlewareError from './middleware/middlewareError';

const app = express();
const db = database.connect(config.db);
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));

const createModels = () => ({
  Book: Book.init(db),
  Author: Author.init(db),
  Customer: Customer.init(db),
  BorrowingHistory: BorrowingHistory.init(db)
});

const createControllers = () => [
  new BookController(app),
  new BorrowingHistoryController(app)
];

const createServices = models => ({
  borrowingHistory: new BorrowingHistoryService(models)
});

const initializeControllers = () => {
  const controllers = createControllers();
  controllers.forEach((controller) => {
    controller.registerController();
  });
};

const registerDependencies = () => {
  app.locals.models = createModels();
  app.locals.services = createServices(app.locals.models);
};

const initializeAssociation = (models) => {
  models.Book.associate(models);
  models.BorrowingHistory.associate(models);
};

registerDependencies();
initializeAssociation(app.locals.models);
initializeControllers();
app.use(middlewareError);
export default app;
